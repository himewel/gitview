import React from 'react';
import { View } from 'react-native';

import Navigator from './src/Navigator';

export default function App() {
  return (
    <View style={{ flex: 1, color: "#2e2e2e" }}>
      <Navigator />
    </View>
  );
}
