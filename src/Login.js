import React, { useState } from 'react';
import { KeyboardAvoidingView, View, Text, StyleSheet,
  StatusBar, Image, ActivityIndicator, ToastAndroid, Platform } from 'react-native';
import { Button, Input, Icon } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';

import logo from '../assets/Octocat.png';

export default function Login({ navigation }) {
  const [ username, setUsername ] = useState('');
  const [ loading, setLoading ] = useState(false);

  handleLogin = async() => {
    setLoading(true);

    if (username) {
      try {
        let url = `http://api.github.com/users/${username}`;
        let req = await fetch(url);

        if (!req.ok) {
          setLoading(false);
          if (Platform.OS !== 'ios') {
            ToastAndroid.showWithGravity('Usuário não encontrado!', ToastAndroid.LONG, ToastAndroid.BOTTOM);
          }
          return;
        }

        let data = await req.json();

        setLoading(false);
        ToastAndroid.showWithGravity('Foi!', ToastAndroid.SHORT, ToastAndroid.BOTTOM);

        navigation.navigate('Main', { data });
        return;

      } catch(err) {
        console.log(err);
        setLoading(false);
        if (Platform.OS !== 'ios') {
          ToastAndroid.showWithGravity('Falha na conexão :(', ToastAndroid.LONG, ToastAndroid.BOTTOM);
        }
        return;
      }
    }

    setLoading(false);
    if (Platform.OS !== 'ios') {
      ToastAndroid.showWithGravity('cade o usuário? >:(', ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
    return;
  }

  return (
    <KeyboardAvoidingView
      behavior='padding'
      style={styles.container}
      enabled >
      <View style={styles.card}>
        <Image source={logo} style={styles.logo} />
        { (!loading)
          ? (
            <View>
              <Input
                label={"Github username"}
                placeholder="Digite seu usuário"
                autoCapitalize={'none'}
                value={username}
                onChangeText={(username) => setUsername(username)}
                onSubmitEditing={handleLogin}
                labelStyle={{ color: 'black', marginBottom: 5, }}
                inputContainerStyle={styles.containerInputStyle}
                inputStyle={styles.inputStyle} />
              <Button
                title="ENTRAR"
                buttonStyle={styles.buttonStyle}
                onPress={handleLogin} />
            </View>
          )
          : (
            <ActivityIndicator
              size={'large'}
              color={'purple'}
              style={{ margin: 50 }} />
          )
        }

      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2e2e2e',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
  },
  card: {
    flexDirection: 'column',
    backgroundColor: 'white',
    marginHorizontal: 10,
    borderRadius: 10,
    padding: 10,
  },
  logo: {
    width: 150,
    height: 150,
    margin: 20,
    marginHorizontal: 70,
    alignSelf: 'center',
  },
  containerInputStyle: {
    borderWidth: 1,
    borderColor: '#AAA',
    borderRadius: 5,
    width: '100%',
  },
  inputStyle: {
    paddingHorizontal: 10,
  },
  buttonStyle: {
    padding: 10,
    backgroundColor: '#292961',
    margin: 10,
    marginTop: 15,
  }
});
