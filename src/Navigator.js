import {createStackNavigator, createAppContainer} from 'react-navigation';

import Login from './Login';
import Main from './Main';

const Navigator = createStackNavigator({
  Login: { screen: Login },
  Main: { screen: Main },
},{
  initialRouteName: 'Login',
  defaultNavigationOptions: {
    header: null,
  },
});

export default createAppContainer(Navigator);
