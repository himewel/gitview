import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, Image,
  TouchableOpacity, ActivityIndicator, Platform } from 'react-native';
import { Badge } from 'react-native-elements';

export default function Main ({ navigation }){
  const [ showFollowers, setShowFollowers ] = useState(false);
  const [ loadingFollowers, setLoadingFollowers ] = useState(false);
  const [ followers, setFollowers ] = useState([]);

  const [ showRepos, setShowRepos ] = useState(false);
  const [ loadingRepos, setLoadingRepos ] = useState(false);
  const [ repos, setRepos ] = useState([]);

  const data = navigation.getParam('data');

  useEffect(() => {
    setFollowers([]);
    if (showFollowers === true) {
      setLoadingFollowers(true);

      // baixa dados
      getFollowers = async(url) => {
        try {
          let req = await fetch(url);
          let data = await req.json();

          setFollowers(data);
          setLoadingFollowers(false);

        } catch(err) {
          console.log(err);
          setShowFollowers(true);
          if (Platform.OS !== 'ios') {
            ToastAndroid.showWithGravity('Aconteceu algum erro!', ToastAndroid.LONG, ToastAndroid.BOTTOM);
          }

        }
      };

      getFollowers(data.followers_url);
    }
  }, [ showFollowers ]);

  useEffect(() => {
    setRepos([]);
    if (showRepos === true) {
      setLoadingRepos(true);

      // baixa dados
      getRepos = async(url) => {
        try {
          let req = await fetch(url);
          let data = await req.json();

          setRepos(data);
          setLoadingRepos(false);

        } catch(err) {
          console.log(err);
          setShowRepos(true);
          if (Platform.OS !== 'ios') {
            ToastAndroid.showWithGravity('Aconteceu algum erro!', ToastAndroid.LONG, ToastAndroid.BOTTOM);
          }

        }
      };

      getRepos(data.repos_url);
    }
  }, [ showRepos ]);

  return (
    <ScrollView style={styles.body}>
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: data.avatar_url }}/>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
          <Text style={styles.name}>{data.name}</Text>
          <Text style={styles.login}>{data.login}</Text>
        </View>
        <Text style={styles.company}>{data.company}</Text>
        <Text style={styles.bio}>{data.bio}</Text>
        <TouchableOpacity
          style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', marginVertical: 10 }}
          onPress={() => {let a = !showFollowers; setShowFollowers(a)}} >
          <Text style={styles.followersButton}>Followers </Text>
          <Badge
            badgeStyle={styles.badge}
            textStyle={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}
            value={data.followers} />
        </TouchableOpacity>
        { (showFollowers) && (
          <View style={styles.dynamicContainer}>
            { (loadingFollowers)
              ? (<ActivityIndicator
                style={{ width: 20, height: 20, alignSelf: 'center' }}
                size={'large'}
                color={'purple'} />)
              : (followers.length === 0 || followers === undefined)
              ? (<Text style={styles.bio}>Nenhum seguidor</Text>)
              : (followers.map((item,index) => (<Text key={index} style={styles.company}>{item.login}</Text>)))
            }
          </View>
        )}
        <TouchableOpacity
          style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', marginBottom: 10  }}
          onPress={() => {let a = !showRepos; setShowRepos(a)}} >
          <Text style={styles.followersButton}>Repositórios </Text>
          <Badge
            badgeStyle={styles.badge}
            textStyle={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}
            value={data.public_repos} />
        </TouchableOpacity>
        { (showRepos) && (
          <View style={styles.dynamicContainer}>
            { (loadingRepos)
              ? (<ActivityIndicator
                style={{ width: 20, height: 20, alignSelf: 'center'}}
                size={'large'}
                color={'purple'} />)
              : ((repos.length === 0 || repos === undefined) && !loadingRepos)
                ? (<Text style={styles.bio}>Nenhum seguidor</Text>)
                : (repos.map((item,index) =>
                    (<View key={index}>
                      <Text style={styles.company}>{item.name}</Text>
                      <Text style={[styles.bio,{textAlign: 'left', marginBottom: 5}]}>{item.description}</Text>
                    </View>)
                  ))
            }
          </View>
        )}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: '#2e2e2e',
    padding: 20,
    paddingTop: 50,
  },
  container: {
    flex: 1,
    backgroundColor: '#eee',
    borderRadius: 10,
    padding: 20,
    marginBottom: 90,
  },
  image: {
    width: '100%',
    height: 300,
    borderRadius: 10,
    marginBottom: 20,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  company: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'gray',
    marginBottom: 5,
  },
  login: {
    fontSize: 16,
    fontWeight: '300',
  },
  bio: {
    fontSize: 14,
    color: 'gray',
    textAlign: 'center'
  },
  followersButton: {
    fontSize: 16,
    textAlign: 'left',
    fontWeight: 'bold',
  },
  badge: {
    backgroundColor: 'black',
    paddingHorizontal: 2,
    paddingVertical: 10,
  },
  dynamicContainer: {
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 5,
    padding: 10,
    width: '100%',
    marginBottom: 10,
  }
});
